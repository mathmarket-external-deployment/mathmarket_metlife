# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 15:19:35 2019

@author: TheMathCompany
"""

################################# Math Market #################################

#Importing packages
try:
    import pandas as pd
except ImportError:
    print("pandas module not imported")   
try:
    from IPython.core.interactiveshell import InteractiveShell
    InteractiveShell.ast_node_interactivity = "all"
except Exception as e:
    print(e)

################################################################################################################################
##FUNCTION: Create dummy variables for categorical columns
##INPUT: df - Data Frame(ADS), dep_var - Dependent variable(String variable eg. <dep_var = "CARAVAN">)
##OUTPUT: Returns a dataframe with continuous columns and dummy values for categorical columns
################################################################################################################################

def createDummies(df,dep_var):
    """
createDummies - Function to create dummies for categorial variables in the dataset
=======================================================================================

**createDummies** is a Python function built by MathMarket of TheMathCompany,
to seperate dependent variable from the dataframe and create dummies.

The module is built using pandas.get_dummies from pandas package.
Therefore, pandas need to be installed in order to use the function.

The parameters that go into the function are - dataframe, dependent variable

"""
    try:
        dep = df[dep_var]
        temp = df.drop(dep_var, axis = 1)
    except Exception as e:
        print('Error occured while trying to drop dependent variable')
        print(e)
    try:
        temp = pd.get_dummies(temp, drop_first = True)
    except Exception as e:
        print('Error occured while trying to generate dummy variables')
        print(e)
    try:
        dummy_data = pd.concat([temp,dep], axis = 1)
    except Exception as e:
        print('Error occured while trying to append dummy varaibles and dependent variable')
        print(e)
    return(dummy_data)