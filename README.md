# Function to create dummy variables
<table>
  <tr><td>
 <b>FUNCTION</b><td> Create dummy variables for categorical columns
  </td></td></tr>
<tr><td>
<b>INPUT</b><td> df - Data Frame(ADS), dep_var - Dependent variable(String variable)
</td></td></tr>
<tr><td>
<b>OUTPUT</b><td> Returns a dataframe with continuous columns and dummy values for categorical columns
</td></td></tr>
<tr><td>
<b>USAGE</b><td> createDummies(df,dep_var)
</td></td></tr>
<tr><td>
<b>DETAILS</b><td>Function to create dummies for categorial variables in the dataset is a Python function built by MathMarket of TheMathCompany,to seperate dependent variable from the dataframe and create dummies.
The module is built using "pandas.get_dummies" from pandas package. Therefore, pandas need to be installed in order to use the function. The parameters that go into the function are - dataframe, dependent variable
</td></td></tr>
</table>
